
# Pyomeca musculoskeletal models

<p><img src="https://raw.githubusercontent.com/pyomeca/biorbd_design/main/logo_png/biorbd_full.png" width="300px">
<img src="https://raw.githubusercontent.com/pyomeca/biorbd_design/main/logo_png/bioviz_full.png" width="300px"></p>

A collection of musculoskeletal models that are to be used with [pyomeca](https://github.com/pyomeca) solftware, particularly wiht [biorbd](https://github.com/pyomeca/biorbd) and [bioviz](https://github.com/pyomeca/bioviz)

## How to use
If you already have `biorbd` and/or `bioviz` installed then just download it to your PC and it is ready to go. You can also clone it using:
```
git clone git@gitlab.inria.fr:auctus-team/components/modelisation/humanmodels/pyomeca_models.git
```
> If you want to copy a specific model and not use the complete repository just make sure to copy the `Geometry` folder with it as well.



## Model list
<img src='images/mobl-arms.png' height="250px">
<img src='images/BrasComplet.png' height="250px">
<img src='images/arm26.png' height="250px">

```
.
├── arm26.bioMod
├── BrasComplet_4DOF.bioMod
├── BrasComplet_5DOF.bioMod
├── BrasComplet.bioMod
├── BrasCompletKinect.bioMod
├── BrasViolon.bioMod
├── MOBL_ARMS_fixed_33_4DOF.bioMod
├── MOBL_ARMS_fixed_33_4DOF_nomuscles.bioMod
├── MOBL_ARMS_fixed_33.bioMod
└── MOBL_ARMS_fixed_33_nohand.bioMod
```
